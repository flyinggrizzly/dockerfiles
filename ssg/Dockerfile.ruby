FROM ruby:2.5.1-stretch

LABEL maintainer='sn@grz.li'
LABEL ruby_version='2.5.1'
LABEL rake_version='12.3.0'

RUN apt-get -y update && \
    apt-get install --assume-yes openjdk-8-jre

RUN gem install s3_website
RUN s3_website install
