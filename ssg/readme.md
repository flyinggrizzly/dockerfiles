# Static site generator dockerfiles for CI

These Docker images include Ruby for Rake tasks, and JRE 8 so the excellent
[`s3_website` gem](https://github.com/laurilehmijoki/s3_website) will be able to
deploy.

## Images available

```
flyinggrizzly/ssg:ruby
flyinggrizzly/ssg:ruby-2.5.1 # same as above, just version named
flyinggrizzly/ssg:jekyll     # same as above, but with tag to clarify usage intent in CI

flyinggrizzly/ssg:hugo      # based on ssg:ruby, but includes Hugo
flyinggrizzly/ssg:hugo-0.53 # same as above, just version named
```

The only difference between the Hugo and Jekyll images is that the Hugo image
includes `wget` and Hugo 0.53. The Jekyll image does not include Jekyll
prepackaged--specify the version you need in a Gemfile. (Hugo is versioned
because the binary has to be preinstalled)
